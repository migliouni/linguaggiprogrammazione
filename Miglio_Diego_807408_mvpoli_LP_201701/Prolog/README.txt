mvpoli.pl

Introduzione:
Questa libreria permette di manipolare polinomi multivariati attraverso una serie di predicati successivamente descritti.
Genericamente queste operazioni permettono di ottenere informazioni riguardo il polinomio di interesse, effettuare calcoli di vario genere tra polinomi oppure convertire
delle espressioni che denotano dei polinomi nella rappresentazione interna del sistema.

Come caricare la libreria:
SWI-Prolog 7.2.3
Esistono due modi per caricare la libreria in SWI-Prolog:
1. Utilizzare dei predicati interagendo direttamente con l'interprete via riga di comando per caricare il file. Utilizzare il predicato working_directory/2 per spostarsi
   nella cartella contenente la libreria. Poi utilizzare il predicato consult/1 per caricare la libreria nel sistema. Anche in questo caso le directory possono seguire le
   convenzioni unix sotto windows. Per dei dubbi su questi predicati fare riferimento alla documentazione di Prolog.
   Esempio: ?- working_directory(D, 'd:/Prolog').	 ?- consult(mvpoli). 	      oppure ?- consult(mvpoli.pl).
2. Utilizzare l'interfaccia grafica. Per farlo bisogner� aprire il file della libreria attraverso il percorso: File->Edit ... quindi si apre l'editor contenente il codice della
   libreria(si prega di non modificarlo) allora bisogner� utilizzare il comando compile buffer (che si trova sotto la tendina compile) presente nell'editor per compilare la libreria.
   Allora si potr� utilizzare la libreria attraverso l'interprete.


Specifiche della libreria

Rappresentazione interna:
Variabili: sono termini del tipo v(Power, VarSymbol) dove Power � l'esponente(maggiore o uguale a zero) a cui � elevato VarSymbol e VarSymbol � il simbolo di variaible, questo pu�
	   essere una sola lettere oppure un qualsiasi simbolo (o combinazione di essi) che soddisfa il predicato atom(VarSymbol). Allora, per esempio, anche y4y pu� essere
	   un simbolo di variabile, poich� soddisfa il predicato atom.
Monomi: sono termini siffatti m(Coefficient, TotalDegree, VarsPowers) dove Coefficient � il coefficiente del monomio, TotalDegree � il grado totale e deve essere maggiore o uguale
	a zero e VarsPowers che � la lista di variaibili che compongono il monomio e sono del tipo precedentemente descritto.
Polinomi: sono termini pi� semplici del tipo poly(Monomials) dove Monomials � la lista dei monomi che compongono il polinomio.

Funzioni:
Si noti che per tutte le scelte implementative e algoritmiche bisogna fare riferimento ai commenti scritti nel codice della libreria.

is_varpower(Variable)
  risulta true quando Variable � una variabile scritta nella rappresentazione interna.
  	  Esempi: 3 ?- is_varpower(v(4, a)).
	  	    true.
		  4 ?- is_varpower(v(4, a3df)).
		    true.
		  5 ?- is_varpower(v(4, 3)).
		    false.
is_monomial(Monomial)
  risulta true quando Monomial � un monomio scritto nella rappresentazione interna. Non effettua controlli numerici sul TotalDegree e l'effettiva somma degli esponenti
  delle variabili. Questa � una funzione che semplicemente controlla se il termine rispetta la rappresentazione interna da un punto di vista simbolico e non matematico.
  Da notare inoltre che non fa nessun tipo di controllo sul coefficiente, altri predicati al loro interno si preoccuperanno di effettuare controlli sul coefficiente. 
  	Esempi: 6 ?- is_monomial(m(42, 42, [v(21, a), v(21, asd)])).
		     true.
		7 ?- is_monomial(m(42, 33, [v(21, a), v(21, asd)])).
		     true.
		8 ?- is_monomial(m(a, 33, [v(21, a), v(21, asd)])).
		     true.
		9 ?- is_monomial(m(a, -33, [v(21, a), v(21, asd)])).
		     false.
is_polynomial(Polynomial)
  risulta true quando Polynomial � un polinomio scritto nella rappresentazione interna, e ogni monomio che lo compone � un monomio scritto secondo la rappresentazione interna.
  	  Esempi: 10 ?- is_polynomial(poly([m(3, 4, [v(4, e)]), m(4, 7, [v(3, l), v(4, u)])])).
	  	     	true.
		  11 ?- is_polynomial(ply([m(3, 4, [v(4, e)]), m(4, 7, [v(3, l), v(4, u)])])).
		     	false.
			
as_monomial(Expression, Monomial)
  il predicato as_monomial � vero quando Monomial � il termine che rappresenta il monomio risultante dal "parsinge dell'espressione Expression.
  Expression deve essere un'espressione scritta secondo la scrittura dei termini composti di Prolog. Il monomio ottenuto in rappresentazione interna sar� ordinato in ordine
  lessicografico crescente delle variaibli. Si noti che questo predicato effettua tutte le semplificazioni del caso riguardo il coefficiente(valuta se � uguale a zero)
  o variabili con lo stesso simbolo. Il coefficiente, se presente altrimenti si sottointende che sia 1, pu� essere dato da una qualsiasi espressione la cui computazione
  restituisce un numero e deve essere inserito come primo elemento dell'espressione dopo il simbolo di moltiplicazione.
  Bisogna porre particolare attenzione a quei simboli, tra cui 'e' e 'pi', che la macchina di calcolo prolog (quella invocata col il predicato is) riconosce come simboli
  che pu� valutare e calcolare poich� indicano dei numeri. Quindi se il primo elemento delle variaibili � un di quei simboli bisogner� scirvere nell'espressione il carattere di
  escape '\' seguito dal simbolo in questione, in questo modo il simbolo verr� trattato come fose un simbolo di variaible se invece lo si vuole utilizzare per indicare il valore
  che indica alla macchina baster� scrivere semplicemente il simbolo. Nelle espressioni in cui � presente il carattere di escape � obbligatorio lo spazio tra l'operatore e il
  carattere di escape, invece non bisogna mettere lo spazio tra il carettere di escape e il simbolo. Infine se si vuole scrivere -e utilizzando e come simbolo di variaible la
  scrittura dell'espressione sar� del tipo: -(\e) oppure esplicitando -1 * \e. Si noti che questo problema si pone solo se il simbolo � il primo elemento tra le variaibili e il
  coefficiente.
    	      Esempi: 12 ?- as_monomial(-e, M).
	      	      	    M = m(-2.718281828459045, 0, []).  
		      13 ?- as_monomial(-(\e), M).
		      	    M = m(-1, 1, [v(1, e)]).
		      14 ?- as_monomial(3 * e + 4, M).
		      	    M = m(12.154845485377136, 0, []).
		      15 ?- as_monomial(3 * \e + 4, M).
		      	    false.
		      16 ?- as_monomial(3 * \e * e * pi * a ^ 0, M).
		      	    M = m(3, 3, [v(2, e), v(1, pi)]).
		      17 ?- as_monomial(3 * \e * e * pi * a ^ 0 * b ^ 42 * c, M).
		      	    M = m(3, 46, [v(42, b), v(1, c), v(2, e), v(1, pi)]).
as_polynomial(Expression, Polynomial)
  il predicato as_polynomial � vero quando Polynomial � il termine che rappresenta il polinomio risultante dal "parsing" dell'espressione Expression. Anche in questo caso Expression
  deve essere un'espressione scritta secondo le "regole di scrittura" dei termini composti di Prolog. I monomi che compongono il polinomio saranno ordinati in ordine crescente
  in base ai loro rispettivi total-degree con eventuali spareggi determinati dalle variaibli che li compongono. Inoltre se due monomi sono composti dalle stesse variaibli
  l'ordinamento � in ordine crescente rispetto alle combinazioni variabile/esponente. Anche in questo caso il polinomio risultante avr� subito tutte le semplificazioni del caso.
  L'utilizzo dei caratteri di escape non cambia.
       	      Esempi: 18 ?- as_polynomial(42, P).
	      	      	    P = poly([m(42, 0, [])]).
		      19 ?- as_polynomial(42 + 3 * 4 * a * f ^ 9 - 3 * 4 *5 + 3 *x - 2 * x, P).
		      	    P = poly([m(-18, 0, []), m(1, 1, [v(1, x)]), m(12, 10, [v(1, a), v(9, f)])]).

Tutti predicati elencati di seguito accettano al posto di Poly sia polinomi e monomi in rappresentazione interna ma anche espressioni di Prolog che denotano polinomi o monomi.

pprint_polynomial(Polynomial)
  il predicato pprint_polynomial risulta vero dopo aver stampato (sullo "standard output") una rappresentazione tradizionale del termine polinomio associato a Polynomial.
     	      Esempio: 22 ?- as_polynomial(42 + 3 * 4 * a * f ^ 9 - 3 * 4 *5 + 3 *x - 2 * x, P),
	      	      	    pprint_polynomial(P).
			    -18 + 1 * x + 12 * a * f^9 
			    P = poly([m(-18, 0, []), m(1, 1, [v(1, x)]), m(12, 10, [v(1, a), v(9, f)])]).
coefficients(Poly, Coefficients)
  risulta vero quando Coefficients e una lista dei coefficienti di Poly. I coefficienti sono nell'ordine in cui essi appaiono nella rappresentazione interna del
  polinomio.
     	      Esempio: 23 ?- coefficients(3 * a + 4 * 5 * y * u * i - cos(3) * x ^ 3, C).
	      	       	     C = [3, 20, 0.9899924966004454].
variables(Poly, Variables)
  risulta vero quando Variables � una lista dei simboli di variabile che appaiono in Poly. Le variabili sono ordinate in ordine lessicografico crescente e le
  ripetizioni di simboli di variabile sono omessi.
     	      Esempio: 26 ?- variables(3 * a + 4 * 5 * a * a * y * u * i - cos(3) * x ^ 3 * ab * a ^ 2, V).
	      	       	     V = [a, ab, i, u, x, y].
monomials(Poly, Monomials)
  risulta vero quando Monomials � la lista dei monomi che appaiono in Poly. I monomi appariranno nell'ordine in cui essi sono presenti in poly, quindi sono ordinati secondo
  le specifiche dell'ordinamento di as-polynomial.
     	      Esempio: 27 ?- monomials(3 * a + 4 * 5 * a * a * y * u * i - cos(3) * x ^ 3 * ab * a ^ 2, Ms).
	      	       	     Ms = [m(3, 1, [v(1, a)]), m(20, 5, [v(2, a), v(1, i), v(1, u), v(1, y)]),
			     	  m(0.9899924966004454, 6, [v(2, a), v(1, ab), v(3, x)])].
			L'output � stato mandato a capo per leggibilit�.
maxdegree(Poly, Degree)
  risulta vero quando Degree � il massimo grado dei monomi che appaiono in Poly.
     	      Esempio: 28 ?- maxdegree(3 * a + 4 * 5 * y * u * i - cos(3) * x ^ 42, D).
	      	       	     D = 42.
mindegree(Poly, Degree)
  risulta vero quando Degree � il minimo grado dei monomi che appaiono in Poly.
     	      Esempio: 29 ?- mindegree(3 * a + 4 * 5 * y * u * i - cos(3) * x ^ 42, D).
	      	       	     D = 1.
polyplus(Poly1, Poly2, Result)
  risulta vero quando Result � il polinomio somma di Poly1 e Poly2. Il polinomio somma � ancora rappresentato in rappresentazione interna, se lo si vuole in forma tradizionale
  bisogner� stampare il risultato di polyplus attraverso il predicato pprint_polynomial/1. Il polinomio ottenuto rispetta ancora le specifiche di ordinamento.
  	    Esempio: 32 ?- polyplus(3 * x * y * v + 42 * a ^ 0 * f, 42 + 3 * f + 4 * x * v * y, R),
	    	     	   pprint_polynomial(R).
			   42 + 45 * f + 7 * v * x * y 
			   R = poly([m(42, 0, []), m(45, 1, [v(1, f)]), m(7, 3, [v(1, v), v(1, x), v(1, y)])]).
polyminus(Poly1, Poly2, Result)
  risulta vero quando Result � il polinomio differenza di Poly1 e Poly2. Il polinomio differenza � ancora rappresentato in rappresentazione interna, se lo si vuole in forma
  tradizionale bisogner� stampare il risultato di polyminus attraverso il predicato pprint_polynomial/1. Il polinomio ottenuto rispetta ancora le specifiche di ordinamento.
  	       Esempio: 33 ?- polyminus(3 * x * y * v + 42 * a ^ 0 * f, 42 + 3 * f + 4 * x * v * y, R),
	       		      pprint_polynomial(R).
			      -42 + 39 * f -1 * v * x * y 
			      R = poly([m(-42, 0, []), m(39, 1, [v(1, f)]), m(-1, 3, [v(1, v), v(1, x), v(1, y)])]).
polytimes(Poly1, Poly2, Result)
  risulta vero quando Result � il polinomio risultante dalla moltiplicazione di Poly1 e Poly2. Il polinomio prodotto � ancora rappresentato in rappresentazione interna,
  se lo si vuole in forma tradizionale bisogner� stampare il risultato di polytimes attraverso il predicato pprint_polynomial/1. Il polinomio ottenuto rispetta ancora
  le specifiche di ordinamento.
     		Esempi: 34 ?- polytimes(3 * f + 2 * a ^ 3, a * b + 3 * f + 4 * a ^ 3, R),
			      pprint_polynomial(R).
			      9 * f^2 + 3 * a * b * f + 18 * a^3 * f + 2 * a^4 * b + 8 * a^6 
			      R = poly([m(9, 2, [v(2, f)]), m(3, 3, [v(1, a), v(1, b), v(1, f)]), m(18, 4, [v(3, a), v(1, f)]),
			      	  	m(2, 5, [v(4, a), v(1, b)]), m(8, 6, [v(6, a)])]).
polyval(Polynomial, VariableValues, Value)
  risulta vero quando Value contiene il valore del polinomio Polynomial nel punto n-dimensionale rappresentato dalla lista VariableValues che contiene un valore per ogni
  variabile ottenuta con il predicato variables/2.
  		 Esempio: 36 ?- polyval(3 * a * x + (42 - 40) * y ^ 3 * x, [2, 3, 1], V).
		 	     	V = 24.
