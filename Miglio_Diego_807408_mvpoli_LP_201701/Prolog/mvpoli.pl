%%%% 807408 Miglio Diego
%%%% 808027 Papetti Daniele Maria

%%%% -*- Mode: Prolog -*-

%%%% mvpoli.pl --

%% Definizione monomio.
% m(Coefficient, TotalDegree, VarsPowers)

is_monomial(m(_C, TD, VPs)) :-
	integer(TD),
	TD >= 0,
	is_list(VPs).

%% Definizione lista variabili con potenza(VPs).
% v(Power, VarSymbol)

is_varpower(v(Power, VarSymbol)) :-
	integer(Power),
	Power >= 0,
	atom(VarSymbol).

%% Definizione polinomio.
% poly(Monomials), cio� un polinomio � una lista di monomi

is_polynomial(poly(Monomials)) :-
	is_list(Monomials),
	foreach(member(M, Monomials), is_monomial(M)).

%% Predicati di semplificazione.
% Semplifica una lista di monomi, trattandola come se fossero i
% componenti di un polinomio. Non scrive i monomi(quindi li elimina
% dalla lista) con coefficiente zero e non riscrive(quindi li elimina
% dalla lista) le variabili elevate alla zero, perch� sarebbe come
% moltiplicare per 1. DA NOTARE che se si vogliono utilizzare questi
% predicati per semplificare dei polinomi bisogner� sfrutta
% l'unificazione Poly = poly(Monimials) e richiamarli su Monomials.
% check_coeff viene utilizzato per la semplificazione dei monomi in
% as_monomial.
% simplify lascia fare "il vero lavoro" a simplify_c/2 e simplify_p/2,
% il primo controlla i coefficienti il secondo, invece, sfrutta
% simplify_v/2 e sum_power/2 per fare i cotrolli di esponenti a zero e
% di somma degli esponenti di potenze messe in prodotto tra loro con la
% stessa base e diverso esponente.
% sum_power/2 a sua volta richiama sum_power/4 che � il predicato che fa
% effettivamente il lavoro di semplificare le potenze con stessa base
% sommandone gli esponenti. I controlli su C =< 0 e C >= 0 sono fatti
% per accorpare i casi in cui il coeffieciente sia 0, 0.0, -0 o -0.0
% altrimenti si sarebbero dovuti scrivere 4 casi per semplificare il
% monomio nel monomio che rappresenta lo zero nella nostra
% rappresentazione interna.
%
% Si possono utilizzare, ovviamente, per semplificare un singolo monomio
% passando come argomento, nella lista, il singolo monomio.

check_coeff(m(C, _, _), m(0, 0, [])) :-
	C =< 0,
	C >= 0,
	!.
check_coeff(m(C, TD, Vars), m(C, TD, Vars)) :- !.

simplify([], []) :- !.
simplify(Mons, SMons) :-
	is_list(Mons),
	simplify_c(Mons, PSMons),
	simplify_p(PSMons, SMons),
	!.

simplify_c([], []) :- !.
simplify_c([m(C, _TD, _Vars) | RMons], SRMons) :-
	C =< 0,
	C >= 0,
	simplify_c(RMons, SRMons),
	!.
simplify_c([m(C, TD, Vars) | RMons],
	   [m(C, TD, Vars) | SRMons]) :-
	simplify_c(RMons, SRMons),
	!.

simplify_p([], []) :- !.
simplify_p([m(C, TD, Vars) | RMons],
	     [m(C, TD, NSVars) | SRMons]) :-
	simplify_v(Vars, SVars),
	sum_power(SVars, NSVars),
	simplify_p(RMons, SRMons),
	!.

simplify_v([], []) :- !.
simplify_v([v(0, _SV) | Vars], SVars) :-
	simplify_v(Vars, SVars),
	!.
simplify_v([v(P, SV) | Vars], [v(P, SV) | SVars]) :-
	simplify_v(Vars, SVars),
	!.

sum_power([], []) :- !.
sum_power([P | Ps], [SP | SPs]) :-
	sum_power(Ps, P, RPs, SP),
	sum_power(RPs, SPs),
	!.
sum_power([], v(P, S), [], v(P, S)) :- !.
sum_power([v(P1, S1) | Vps], v(P, S),
	  [v(P1, S1) | RVps], TP) :-
	S \= S1,
	sum_power(Vps, v(P,S), RVps, TP),
	!.
sum_power([v(P1, S) | Vps], v(PP, S),
	  RVps, TP) :-
	NP is P1 + PP,
	sum_power(Vps, v(NP,S), RVps, TP),
	!.

% sum_similar(List1, List2), risulta vero quando List2 � la lista dei
% monomi di List1 in cui sono stati sommati tra loro i monomi simili.

sum_similar([], []) :- !.
sum_similar([M | Mons], [ SM | SMons]) :-
	sum_similar(Mons, M, RMons, SM),
	sum_similar(RMons, SMons),
	!.
sum_similar([], m(PC, TD, Vars), [], m(PC, TD, Vars)) :-
	!.
sum_similar([m(C, TD1, Vars1) | Mons],
	     m(PC, PTD, PVars),
	     [m(C, TD1, Vars1) | RMons], TS) :-
	Vars1 \= PVars,
	sum_similar(Mons, m(PC, PTD, PVars),
		     RMons, TS),
	!.
sum_similar([m(C, TD, Vars1) | Mons],
	     m(PC, TD, Vars2),
	     RMons, TS) :-
	Vars1 = Vars2,
	NPC is C + PC,
	sum_similar(Mons, m(NPC, TD, Vars1), RMons, TS),
	!.

%% as_monomial(Expression, Monomial)
% Risulta vero quando Monomial � la rappresentazione interna del monomio
% espresso in Expression.
% Sfrutta una serie di predicati di appoggio spiegati di seguito.

as_monomial(Exp, NM) :-
	parse_exp_m(Exp, Vars, C),
	parse_var(Vars, Powers, VarSymbol),
	calculate_TD(Powers, TD),
	build_tmp(VarSymbol, Powers, Tmp),
	msort(Tmp, STmp),
	reverse_tmp(STmp, VPs),
	M = m(C, TD, VPs),
        check_coeff(M, PM),
	PM = m(PC, PTD, PVars),
	simplify_v(PVars, NPVars),
	sum_power(NPVars, SVars),
	NM = m(PC, PTD, SVars),
	is_monomial(NM).

% Predicati di appoggio
%
% parse_exp_m/3, "restituisce" una lista di variabili con il possibile
% esponente e il coefficiente C come terzo parametro. Il primo caso
% sfrutta il predicato compute_if_possible/2 che risulta vero quando
% l'espressione � calcolabile, di fatto questo � il caso base quando il
% monomio presenta un qualsiasi coefficiente calcolabile dalla macchina
% prolog. Il secondo caso, invece, � utilizzato per la gestione dei casi
% del tipo -x o -x ^ 3. Il terzo invece � il caso ricorsivo per il
% parsing del monomio nel caso in cui la parte rimasta non sia
% calcolabile, e quindi si suppone che ci siano ancora variabili con
% l'esponente. Gli ultimi sono casi base superflui nella maggior parte
% dei casi.
% Si noti che l'utilizzo di compute_if_possible porta alla conseguenza
% che se come primo carattere dell'espressione o come primo elemento
% nella "serie di variabili"(es: e * a ^ 3 o 3 * e * a ^ 3) che si vuole
% trasformare in monomio � un simbolo a cui � associato un valore
% particolare dalla macchina( ad esemprio 'pi' con il valore pigreco o
% 'e' con il numero di nepero) questo simbolo verr� trattato come valore
% dalla macchina e lo considerer� un coefficiente, calcolandolo. Per
% evitare ci� si introducono il carattere di escape '\' che � da
% utilizzare ogni volta che il primo elemento della serie di variabili
% del monomio � un simbolo che la macchina associa ad un valore.
% Esempio: 3 * \e * a ^ 3, e viene trattata come variabile,
% invece 3 * e * a^3 viene calcolato come coefficiente 3*e.

parse_exp_m(E1, [], X) :-
	compute_if_possible(E1, X),
	!.
parse_exp_m(E1, Vs, NC) :-
	compound(E1),
	functor(E1, F, 1),
	F = -,
	arg(1, E1, V),
	parse_exp_m(V, Vs, C),
	NC is -C,
	!.
parse_exp_m(E1*E2, [E2 | Es], C) :-
	parse_exp_m(E1, Es, C),
	!.
parse_exp_m(E1, [], E1) :-
	number(E1),
	!.
parse_exp_m(E1, [E1], 1) :-
	compound(E1),
	!.
parse_exp_m(E1, [E1], 1) :-
	atom(E1),
	!.

% compute_if_possible/2, fallisce, attraverso la gestione delle
% eccezioni, quando Exp non � calcolabile, se invece lo � X sar� il
% valore calcolato da Exp. La gestione delle eccezioni � stata
% necessaria per due motivi: il primo, pi� banale, per accettare senza
% troppi controlli coefficienti del genere n1 * n2, dove n1 e n2 sono
% valori calcolabili dal sistema. Il secondo perch� cos� facendo si
% possono testare anche espressioni su cui non si effetuano eccessivi
% controlli specifici in maniera veloce e comoda da scrivere evitando,
% appunto, ulteriori casi in parse_exp_m/3. Perch� gestendo
% l'eccezione e facendo tornare false l'esecuzione non si interrompe,
% poich� l'eccezione viene gestita, e quindi si possono continuare i
% test su altri casi cosa che non sarebbe possibile in maniera cos�
% agile senza la gestione dell'eccezione.

compute_if_possible(Exp, X) :-
	catch(X is Exp, _, fail).

% parse_var/3, "restituisce" due liste associate di variabile esponente,
% con gli esponenti >= 0. Il secondo caso � utilizzato per gestire i
% caratteri di escape.

parse_var([], [], []) :- !.
parse_var([\V | Vs], Ps, Vars) :-
	parse_var([V | Vs], Ps, Vars),
	!.
parse_var([V | Vs], [1 | Ps], [V | Vars]) :-
	atom(V),
	parse_var(Vs, Ps, Vars),
	!.
parse_var([V ^ N | Vs], [N | Ps], [V | Vars]) :-
	atom(V),
	integer(N),
	N >= 0,
	parse_var(Vs, Ps, Vars),
	!.

% calculate_TD/2, vero quando TD � la somma della lista degli esponenti.

calculate_TD([], 0) :- !.
calculate_TD([P | Ps], TD) :-
	calculate_TD(Ps, Tmp),
	TD is Tmp + P,
	!.

% build_tmp/3, "restituisce" la lista delle VarPowers con la
% struttura v(VarSymbol, Power) per poter fare l'ordinamento
% lessicografico con msort/2.

build_tmp([], [], []) :- !.
build_tmp([V | Vs], [P | Ps], [v(V, P) | Tmps]) :-
	build_tmp(Vs, Ps, Tmps),
	!.

% reverse_tmp/2, "restituisce" la lista delle VarPowers con la struttura
% v(Power, VarSymbol).

reverse_tmp([], []) :- !.
reverse_tmp([v(V, P) | Vs], [v(P, V) | IVs]) :-
	reverse_tmp(Vs, IVs),
	!.

%% as_polynomial(Expression, Polynomial)
% Risulta vero quando Polynomial � la rappresentazione interna del
% polinomio scritto in Expression, il polinomio � ordinato in ordine
% decrescente di TD e nel caso di parit� di TD gli spareggi sono fatti
% guardando l'ordine lessicografico delle variabili, per fare ci�
% si sfruttano dei predicati di appoggio che, tra le altre cose,
% invertono l'odrinde della rappresentazione interna dei monomi per
% facilitare l'ordinamento. Dopo l'ordinamento risistemano la
% rappresentazione interna come da specifiche.

as_polynomial(Exp, P) :-
	parse_exp_p(Exp, Ms),
	convert_pm(Ms, Monomials),
	negative_tmp(Monomials, NMonomials),
	msort(NMonomials, ONMonomials),
	positive_tmp(ONMonomials, OMonomials),
	simplify(OMonomials, K),
	sum_similar(K, K1),
	simplify(K1, S),
	P = poly(S),
	is_polynomial(P).

% Predicati di appoggio
%
% parse_exp_p/2, "restituisce" la lista dei monomi dall'espressione
% aggiungendo il meno al monomio(E2, elemento dell'espressione) se il
% monomio e il resto dell'espressione sono separati dal meno.

parse_exp_p(E1 + E2, [E2 | Es]) :-
	parse_exp_p(E1, Es),
	!.
parse_exp_p(E1 - E2, [- E2 | Es]) :-
	parse_exp_p(E1, Es),
	!.
parse_exp_p(E1, [E1]) :- !.

% convert_pm(List1, List2), risulta vero quando List2 � la
% rappresentazione interna dei monomi nella lista List1, sfrutta il
% predicato as_monomial/2 precedentemente definito.

convert_pm([], []) :- !.
convert_pm([-(M) | Mons], [m(NC, TD, Vars) | Ms]) :-
	as_monomial(M, m(C, TD, Vars)),
	NC is -C,
	convert_pm(Mons, Ms),
	!.
convert_pm([Mon | Mons], [M | Ms]) :-
	as_monomial(Mon, M),
	convert_pm(Mons, Ms),
	!.

% negative_tmp/2, viene utilizzata per modificare l'ordine della
% rappresentazione interna del monomio in modo da far ordinare a
% msort secondo l'ordine di priorit� secondo la specifica. Sfrutta
% reverse_tmp/2 precedentemente definito, poich� quest'ultimo �
% invertibile, il quale mi permette di invertire l'ordine esponente
% simbolo di ogni variabile dei monomi.

negative_tmp([], []):- !.
negative_tmp([M | Mons], [NM | NMons]) :-
	M = m(C, TD, Vars),
	reverse_tmp(NVars, Vars),
	NM = m(TD, NVars, C),
	negative_tmp(Mons, NMons),
	!.

% positive_tmp/2, sistema i monomi dopo l'ordinamento, procedendo in
% maniera opposta a negative_tmp. Sfrutta reverse_tmp/2 precedentemente
% definita.

positive_tmp([], []) :- !.
positive_tmp([NM | NMons], [M | Mons]) :-
	NM = m(TD, NVars, C),
	reverse_tmp(NVars, Vars),
	M = m(C, TD, Vars),
	positive_tmp(NMons, Mons),
	!.

%% pprint_polynomial(Polynomial)
% Stampa il polinomio secondo la notazione comune, lasciando sottointesi
% gli esponenti 1 ma sono sempre esplicitati i coefficienti 1 e -1.
% Il primo caso viene utilizzato per quando deve stampare un solo
% monomio scritto nella rappresentazione interna, richiamandosi
% ricorsivamente. Il secondo � la stampa vera e propria.
% Per effettuare la stampa si appoggia ai predicati di semplificazione
% per semplificare possibili polinomi passati in forma interna e che non
% sono stati costruiti attraverso as_polynomial/2. Inoltre si appoggia a
% pprint_mon/1 che � quello che effettivamente si fa carico della stampa
% della lista dei monomi e quindi di tutto il polinomio.

pprint_polynomial(M) :-
	is_monomial(M),
	check_coeff(M, SM),
	pprint_polynomial(poly([SM])),
	!.
pprint_polynomial(Polynomial) :-
	is_polynomial(Polynomial),
	Polynomial = poly(P),
	simplify(P, P1),
	sum_similar(P1, P2),
	simplify(P2, P3),
	pprint_mon(P3),
	!.

% Predicati di appoggio
%
% pprint_mon/1, stampa una lista di monomi e per ogni monomio richiama
% write_vars. Inserisce anche i simboli di + o - per "legare" tra di
% lori i vari monomi per la formazione del polinomio.


pprint_mon([]) :- !.
pprint_mon([m(C, _, [])]) :-
	write(C),
	!.
pprint_mon([m(C, _, Vars)]) :-
	write(C),
	write(' * '),
	write_vars(Vars),
	!.
pprint_mon([m(C, _, []), m(C1, _, []) | Ks]) :-
	C1 > 0,
	write(C),
	write(' +'),
	pprint_mon([m(C1, _, []) | Ks]),
	!.
pprint_mon([m(C, _, []), m(C1, _, Vars2) | Ks]) :-
	C1 > 0,
	write(C),
	write(' + '),
	pprint_mon([m(C1, _, Vars2) | Ks]),
	!.
pprint_mon([m(C, _, []), m(C1, _, []) | Ks]) :-
	C1 < 0,
	write(C),
	write(' '),
	pprint_mon([m(C1, _, []) | Ks]),
	!.
pprint_mon([m(C, _, []), m(C1, _, Vars2) | Ks]) :-
	C1 < 0,
	write(C),
	pprint_mon([m(C1, _, Vars2) | Ks]),
	!.
pprint_mon([m(C, _, Vars1), m(C1, _, Vars2) | Ks]) :-
	C1 > 0,
	write(C),
	write(' * '),
	write_vars(Vars1),
	write('+ '),
	pprint_mon([m(C1, _, Vars2) | Ks]),
	!.
pprint_mon([m(C, _, Vars1), m(C1, _, Vars2) | Ks]) :-
	C1 < 0,
	write(C),
	write(' * '),
	write_vars(Vars1),
	pprint_mon([m(C1, _, Vars2) | Ks]),
	!.

% write_vars/2, stampa le variabili(quelle del tipo: v(Power, VarSymbol)
% passate nella lista sottoindendo l'1 come esponente ed esplicitando
% l'operatore di prodotto tra una variabile e altra

write_vars([]) :- !.
write_vars([v(1, Sym)]) :-
	write(Sym),
        write(' '),
	!.
write_vars([v(Pow, Sym)]) :-
	write(Sym),
	write('^'),
	write(Pow),
	write(' '),
	!.
write_vars([v(1, Sym) | Zs]) :-
	write(Sym),
	write(' * '),
	write_vars(Zs),
	!.
write_vars([v(Pow, Sym) | Zs]) :-
	write(Sym),
	write('^'),
	write(Pow),
	write(' * '),
	write_vars(Zs),
	!.

%% Predicati di appoggio che vengono utilizzati per rendere "flessibili"
% gli input dei predicati, cio� per fare in modo che non accettino come
% input solo la rappresentazione interna ma anche espressioni o monomi
% in rappresentazione interna, sono chiamati come prime istruzioni in
% tutti i predicati definiti di seguito.

transform(Poly, Poly) :-
	is_polynomial(Poly),
	!.
transform(m(C, TD, Vars),
	  poly([m(C, TD, Vars)])) :-
	!.
transform(Exp, Poly) :-
	as_polynomial(Exp, Poly),
	!.

%% coefficients(Polynomial, Coefficients)
% Risulta vero quando Coefficients � la lista dei coefficienti dei
% monomi che compongono il polinomio Polynomial. extract_coeff/2 � il
% predicato che si fa carico di estrarre effettivamente i coefficienti e
% quindi restituirli in una lista. Il primo caso � per la gestione di
% quando viene passato un polinomio, monomio o espressione che
% rappresentano lo zero.

coefficients(Poly, [0]) :-
	transform(Poly, NPoly),
	NPoly = poly([]),
	!.
coefficients(Poly, Coefficients) :-
	transform(Poly, NPoly),
	NPoly = poly(P),
	extract_coeff(P, Coefficients),
	!.

% extract_coeff/2, predicato di appoggio che � quello che si carica
% effettivamente del lavoro di estrarre i coefficienti

extract_coeff([], []) :- !.
extract_coeff([m(C1, _, _) | Ms], [C1 | Cs]) :-
	extract_coeff(Ms, Cs).

%% variables(Poly, Variables)
% Risulta vero quando Variables � una lista ordinata dei simboli di
% variabile che appaiono in Poly. Da notare che questa lista deve essere
% senza ripetizioni di elementi per questo viene utilizzato il predicato
% list_to_set/2. I predicati vars ed extract symbol sono quelli che si
% prendono carico effettivamente di estrarre i simboli di variabile.

variables(poly([]), []) :- !.
variables(Poly, OVariables) :-
	transform(Poly, NPoly),
	vars(NPoly, Vars),
	list_to_set(Vars, Variables),
	msort(Variables, OVariables),
	!.

vars(poly([]), []) :- !.
vars(poly([m(_, _, V) | Ms]), Vars) :-
	extract_symbol(V, SymbolsV),
	append(SymbolsV, Symbolss, Vars),
	vars(poly(Ms), Symbolss),
	!.

extract_symbol([], []) :-!.
extract_symbol([v(_, S) | Vs], [S | Ss]) :-
	extract_symbol(Vs, Ss),
	!.

%% monomials(Poly, Monomials)
% Risulta vero quando Monomials � la lista ordinata dei monomi che
% appaiono in Poly.
% Si suppone che se viene passato un polinomio nella
% rappresentazione interna esso sia stato costruito con il predicato
% as_polynomial/2, altrimenti questa lista di monomi non � ordinata.
% Se viene passato un singolo monomio in rappresentazione interna
% chiaramente sar� gi� ordinato poich� la lista sar� composta da un solo
% elemento che �, appunto, il monomio. Invece se viene passata
% un'espressione, richiamando il predicato transform/2 nelle condizioni
% della regola, si avr� un polinomio in rappresentazione interna
% costruito attraverso as_polynomial/2 e quindi ordinato.

monomials(poly([]), []) :- !.
monomials(Poly, Monomials) :-
	transform(Poly, Npoly),
	Npoly = poly(Monomials),
	!.

%% maxdegree(Poly, Degree)
% Risulta vero quando Degree � il massimo grado dei monomi che appaiono
% in Poly.
% Questo predicato considera il caso in cui se venisse passato un
% polinomio nella rappresentazione interna non ordinato riesce comunque
% a trovare il TD maggiore poich� scorre tutta la lista cercando il
% maggiore. Supponendo che il polinomio � gi� ordinato anche nel caso in
% cui venga passato in rappresentazione interna, il grado massimo
% sarebbe l'ultimo elemento della lista dei monomi che compongono il
% polinomio. In questo caso basterebbe un predicato che scorra la lista
% fino all'ultimo elemento e poi, sfruttando l'unificazione, ritorni
% il TD di quell'ultimo monomio.

maxdegree(0,0) :- !.
maxdegree(poly([]), 0) :- !.
maxdegree(Poly, Degree) :-
	transform(Poly, NP),
	NP = poly(Monomials),
	Monomials = [m(_C1, TD1, _Vars1) | MonomialsN],
	extract_maxdegree(MonomialsN, TD1, Degree),
	!.

extract_maxdegree([], Max_tmp, Max_tmp) :- !.
extract_maxdegree([m(_C, TD, _Vars) | Mons], Max_tmp, GTD) :-
	TD > Max_tmp,
	extract_maxdegree(Mons, TD, GTD),
	!.
extract_maxdegree([m(_C, TD, _Vars) | Mons], Max_tmp, GTD) :-
	TD =< Max_tmp,
	extract_maxdegree(Mons, Max_tmp, GTD),
	!.

%% mindegree(Poly, Degree)
% Risulta vero quando Degree � il minimo grado dei monomi che appaiono
% in Poly.
% Si comporta in maniera analoga a maxdegree/2 ma in questo caso
% cercando il minimo. Anche in questo caso � valido un discorso analogo
% rispetto al supporre che se venisse passato un polinomio in
% rappresentazione interna gi� ordinato(poich� costruito attraverso
% as_polynomial) il grado minimo sarebbe il primo elemento della lista e
% quindi basterebbe recuperarlo attraverso l'unificazione.

mindegree(0,0) :- !.
mindegree(poly([]), 0) :- !.
mindegree(Poly, Degree) :-
	transform(Poly, NP),
	NP = poly(Monomials),
	Monomials = [m(_C1, TD1, _Vars1) | MonomialsN],
	extract_mindegree(MonomialsN, TD1, Degree),
	!.

extract_mindegree([], Min_tmp, Min_tmp) :- !.
extract_mindegree([m(_C, TD, _Vars) | Mons], Min_tmp, LTD) :-
	TD < Min_tmp,
	extract_mindegree(Mons, TD, LTD),
	!.
extract_mindegree([m(_C, TD, _Vars) | Mons], Min_tmp, LTD) :-
	TD >= Min_tmp,
	extract_mindegree(Mons, Min_tmp, LTD),
	!.

%% polyplus(Poly1, Poly2, Result)
% Risulta vero quando Result � il polinomio somma di Poly1 e Poly2.
% Sommare due polinomi non vuol dire nient'altro che scrivere tutti i
% monomi dei due polinomi fuori da parentesi senza cambiare i segni ai
% propri coefficienti e poi semplificare il polinomio cos� ottenuto.
% In termini informatici(e quindi a livello di gestione di strutture
% dati e algoritmi) significa appendere tra loro le due liste di
% monomi che compongono Poly1 e Poly2, estraendole sfruttando
% l'unificazione, e in seguito richiamare i predicati di
% semplificazione in questo modo si evitano molte righe di codice che
% implementano la somma tra due polinomi come spesso viene insegnata,
% cio� cercare per ogni monomio del primo polinomio se nel secondo
% polinomio sono presenti monomi simili e nel caso sommarli e di
% seguito scrivere tutti i monomi di ambo i polinomi non sommati.
% Da notare che i casi base in cui si somma un polinomio ad un polinomio
% vuoto(poly([])) sono compresi nel caso "passo". Ovvero nel fatto che
% appendere una lista di monomi ad una lista vuota si ottiene la stessa
% lista di monomi con il vantaggio, che se per caso venga passato un
% polinomio in rappresentazione interna senza che sia stato costruito
% con as_polynomial, e che quindi pu� presentare dei monimi simili o dei
% coefficienti a zero, questi vengano semplificati. Infine sfrutta i
% predicati negative_tmp/2 e positive_tmp/2 per ordinare i polinomi
% ottenuti.

polyplus(Poly1, Poly2, Result) :-
	transform(Poly1, P1),
	transform(Poly2, P2),
	P1 = poly(M1),
	P2 = poly(M2),
	append(M1, M2, Sum),
	simplify(Sum, PSum),
	sum_similar(PSum, NPSum),
	simplify(NPSum, TMResult),
	negative_tmp(TMResult, NTMResult),
	msort(NTMResult, ONTMResult),
	positive_tmp(ONTMResult, MResult),
	Result = poly(MResult).

%% polyminus(Poly1, Poly2, Result)
% Risulta vero quando Result � il polinomio differenza di Poly1 e Poly2.
% Sottrare due polinomi significa sommare il primo polinomio al secondo
% il quale ha i segni di ogni monomi opposti. Per comodit� il predicato
% cambia il segno al coefficiente di ogni monomio del secondo Polynomio
% dopo di che richiama polyplus/3 per effettuare la somma. Si appoggia
% al predicato change_sign/2 per invertire i segni.

polyminus(Poly1, Poly2, Result) :-
	transform(Poly1, P1),
	transform(Poly2, P2),
	P2 = poly(M2),
	change_sign(M2, IM2),
	PI2 = poly(IM2),
	polyplus(P1, PI2, Result).

change_sign([], []) :- !.
change_sign([m(C, TD, Vars) | Mons2],
	      [m(IC, TD, Vars) | IMons2]) :-
	IC is -C,
	change_sign(Mons2, IMons2),
	!.

%% polytimes(Poly1, Poly2, Result)
% Risulta vero quando Result � il polinomio risultante della
% moltiplicazione tra Poly1 e Poly2.
% Il predicato che effettivamente si prende carico di moltiplicare le
% due liste di monomi � product/4 il quale oltre alle due liste di
% monomi tiene un terzo arogmento di "backup" poich� ogni monomio deve
% essere moltiplicato per tutti gli altri. Questo significa arrivare ad
% avere la seconda lista di monomi vuota ma per poter moltiplicare il
% secondo monomio per tutta la lista dei monomi di Poly2 bisogna
% risettare la lista di monomi che viene fatto attraverso l'argomento
% di "backup". Quello che effettivemente fa product � moltiplicare i
% coefficienti e poi appendere le due liste di VarsPowers di ogni
% monomio e poi lasciare l'opera delle eventuali somme degli esponenti
% al predicato simplify/2. I casi base non controllano se Poly1 �
% scritto come espressione o nella rappresentazione interna poich� se
% viene moltiplicato per un polinomio vuoto, o un monomio che ha
% coefficiente zero, il risultato � il polinomio vuoto. Utilizza anche
% qua in modo analogo a polyplus gli stessi predicati per l'ordinamento.
% Da notare che in product � presente un sort per le variabili in ordine
% lessicografico poich� in polytimes vengono ordinati i monomi e non le
% variabili all'interno di ogni monomio. Per fare ci� si sfrutta lo
% stesso metodo usato in as_monomial con reverse_tmp e il vantaggio che
% � un predicato reversibile.

polytimes(poly([]), _Poly1, poly([])) :- !.
polytimes(_Poly1, poly([]), poly([])) :- !.
polytimes(m(0, _, _), _Poly1, poly([])) :- !.
polytimes(_Poly1, m(0, _, _), poly([])) :- !.

polytimes(Poly1, Poly2, Result) :-
	transform(Poly1, P1),
	transform(Poly2, P2),
	P1 = poly(M1),
	P2 = poly(M2),
	product(M1, M2, M2, MR),
	simplify(MR, SMR),
	sum_similar(SMR, NSMR),
	simplify(NSMR, TMResult),
	negative_tmp(TMResult, NTMResult),
	msort(NTMResult, ONTMResult),
	positive_tmp(ONTMResult, MResult),
	Result = poly(MResult),
	!.

product([], _Ms2, _M2, []) :- !.
product([m(_C1, _TD1, _Vars1) | Ms1],
	[], M2, Rs) :-
	product(Ms1, M2, M2, Rs),
	!.
product([m(C1, TD1, Vars1) | Ms1],
	[m(C2, TD2, Vars2) | Ms2],
	M2, [m(C, TD, OVars) | Rs]) :-
	C is C1 * C2,
	TD is TD1 + TD2,
	append(Vars1, Vars2, Vars),
	reverse_tmp(Vars, IVars),
	msort(IVars, OIVars),
	reverse_tmp(OIVars, OVars),
	product([m(C1, TD1, Vars1) | Ms1], Ms2, M2, Rs),
	!.

%% polyval(Polynomial, VariableValues, Value)
% Risulta vero quando Value contiene il valore del polinomio Polynomial
% nel punto n-dimensionale rappresentato dalla lista VariableValues, che
% contiene un valore per ogni variabile ottenuta on il predicato
% variables/2.
% Oltre a vari controlli sul tipo(e quindi in caso trasformazione) di
% modo in cui viene passato Polynomial e su lunghezza tra il risultato
% ottenuto da variables e VariableValues e l'essere effetivamente una
% lista di VariableValues, polyval/3 lascia fare tutto il lavoro di
% sostituzione di valori alle variabili e di calcolo dei valori dei
% singoli monomi e di somma dei valori di ogni monomio a dei predicati
% di appoggio.

polyval(Polynomial, VariableValues, Value) :-
	transform(Polynomial, Poly),
	is_list(VariableValues),
	variables(Poly, Vs),
	length(VariableValues, LVV),
	length(Vs, LV),
	LVV = LV,
	Poly = poly(Ms),
	calculate_monomials(Ms, Vs, VariableValues, Result),
	sum_val(Result, 0, Value),
	!.

% predicati di appoggio
%
% calculate_monomials/4, sfrutta vari predicati di appoggio ricevendo
% come parametri di input la lista di monomi, la lista delle variabili
% ottenute da variables/2 e la lsita dei valori associati e restituisce
% come risultato la lista dei valori calcolati per ogni monomio.
% Quello che fa � dopo aver avuto la lista calcolata di ogni potenza
% appende a questa lista il valore del coefficiente per poi far fare, ad
% un altro predicato, il prodotto tra tutti i valori. Si richiama
% ricorsivamente per ogni monomio della lista che compone il polinomio
% passato in polyval/3.

calculate_monomials([], _Vs, _VV, []) :- !.
calculate_monomials([m(C, _TD, Varsm) | Mss], Vs,
		    VariableValues, [Product | Rs]):-
	substitute_variables(Varsm, Varsm, Vs,
			    VariableValues, SVarsm),
	calculate_variables(SVarsm, ValuesVars),
	append([C], ValuesVars, Factor),
	product_factor(Factor, 1, Product),
	calculate_monomials(Mss, Vs, VariableValues, Rs),
	!.

% substitute_variables/5, sostituisce le variabili coi vaolri
% associati per questo riceve come argomenti la lista delle
% VarsPowers del monomio la lista ottenuta da variables/2 e la lista
% dei valori associati alle variaibili. Quello che fa �: per ogni
% coppia variabile-valore associato scorre la lista delle VarsPowers e
% quando incontra una VarPower con lo stesso simbolo sostituisce al
% simbolo il valore associato. Dovendo scorrere la lista pi� volte il
% predicato necessita di una lista di "backup" che non viene mai toccata
% durante la computazione e che risetta la lista da scorrere ogni volta
% che si cambia la coppia variabile-valore associato.

substitute_variables(_Varsms, _Varsm, [], [], []) :- !.
substitute_variables([], Varsm, [_S | Vss],
		     [_VV | VVs], SVs) :-
	substitute_variables(Varsm, Varsm, Vss, VVs, SVs),
	!.
substitute_variables([v(_P, S) | Vps], Varsm,
		    [DS | Vss], [VV | VVs], SVs) :-
	S \= DS,
	substitute_variables(Vps, Varsm, [DS | Vss],
			    [VV | VVs], SVs),
	!.
substitute_variables([v(P, S) | Vps], Varsm,
		    [S | Vss], [VV | VVs], [v(P, VV) | SVs]) :-
	substitute_variables(Vps, Varsm, [S |Vss],
			    [VV | VVs], SVs),
	!.

% calculate_variables/2, prende in ingresso una lista di variabili
% sostiuite con il loro esponente(come da rappresentazione interna) e ne
% calcola il valore.

calculate_variables([], []) :- !.
calculate_variables([v(P, B) | Vs], [V | Values]) :-
	V is B ^ P,
	calculate_variables(Vs, Values).

% product_factor/3, moltiplica tutti gli elementi della lista
% restituendo un risultato. Ha ariet� 3, come l'ha anche sum_val, perch�
% il valore viene calcolato piano piano(nel parametro Partial) che si
% scorre la lista e quindi necessita di un valore da cui partire,
% ovviamente verr� passato il valore 1 quando viene chiamato questo
% predicato in calculate_monomials/4.

product_factor([], P, P) :- !.
product_factor([F | Fs], Partial, Product) :-
	Tmp is Partial * F,
	product_factor(Fs, Tmp, Product),
	!.

% sum_val/3, esattamente analogo a product_factor ma in questo caso dato
% che si tratta di somma si passa, quando viene chiamato il
% predicato, come primo valore della somma parziale l'elemento neutro
% della somma poich� in questo caso si tratta solo di sommare
% algebricamente i valori di ogni monomio. Riceve, quindi, la lista dei
% valori dei vari monomi che compongono il polinomio Polynomial passato
% in polyval e restituisce la loro somma algebrica.

sum_val([], S, S) :- !.
sum_val([A | As], Partial, Sum) :-
	Tmp is Partial + A,
	sum_val(As, Tmp, Sum),
	!.

%%%% end of file -- mvpoli.pl --













