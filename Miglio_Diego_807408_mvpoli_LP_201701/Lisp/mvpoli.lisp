;;;; 807408 Miglio Diego
;;;; 808027 Papetti Daniele Maria


;;;; -*- Mode: Lisp -*-

;;;; mvpoli.lisp

;;; Definizione di un monomio
;;; (m coefficient total-degree vars-n-powers)

(defun is-monomial (m)
  (and (listp m)
       (eq 'm (first m))
       (let ((mtd (monomial-degree m))
	     (vps (varpowers m))
	     )
	 (and (integerp mtd)
	      (>= mtd 0)
	      (listp vps)
	      (every #'is-varpower vps)))))

;;; Definizione di vars-n-powers
;;; (v power var-symbol)

(defun is-varpower (vp)
  (and (listp vp)
       (eq 'v (first vp))
       (let ((p (varpower vp))
	     (v (varpower-symbol vp))
	     )
	 (and (integerp p)
	      (>= p 0)
	      (symbolp v)))))

;;; Definizione di polinomio
;;; (poly monomials)

(defun is-polynomial (p)
  (and (listp p)
       (eq 'poly (first p))
       (let ((ms (poly-monomials p)))
	 (and (listp ms)
	      (every #'is-monomial ms)))))

;;; varpowers Monomial --> VP-list
;;; Data una struttura Monomial, ritorna la list di varpowers VP-list.

(defun varpowers (m)
  (if (listp m)
      (fourth m)))

;;; monomial-degree Monomial --> TotalDegree
;;; Data una struttura Monomial, ritorna il suo grado totale TotalDegree.

(defun monomial-degree (m)
  (if (listp m)
      (third m)))

;;; varpower Variable --> Power
;;; Data una struttura Variable, ritorna l'esponente.

(defun varpower (v)
  (if (listp v)
      (second v)))

;;; varpower-symbol Variable --> Symbol
;;; Data una struttura variable, ritorna il simbolo della variabile. 

(defun varpower-symbol (v)
  (if (listp v)
      (third v)))

;;; poly-monomials Polynomial --> Monomials
;;; Data una struttura Polynomial, ritorna la sua lista di monomi.

(defun poly-monomials (p)
  (if (listp p)
      (second p)))

;;; vars-of Monomial --> Variables
;;; Data una struttura Monomial, ritorna la lista di variabili
;;; Variables.

(defun vars-of (m)
  (extract-symbol (varpowers m)))

;; extract-symbol VP-list --> Symbol-list
;; Data una lista di vars-n-powers, ritorna la lista di simboli
;; associata.

(defun extract-symbol (vps)
  (if (null vps) nil
    (append (list (third (first vps)))
	    (extract-symbol (rest vps)))))

;;; monomial-coefficient Monomial --> Coefficient
;;; Data una struttura Monomial, ritorna il uso coefficiente
;;; Coefficient.

(defun monomial-coefficient (m)
  (if (listp m)
      (second m)))

;;; Funzioni di semplificazione
;;; Sono analoghe a quelle di Prolog e effettuano esattamente gli
;;; stessi tipi di semplificazioni, che sono quelle possibili sui vari
;;; monomi o polinomi. Anche in questo caso trattano la lista di
;;; monomi che compongono il polinomio, quindi questa lista va
;;; estratta prendendo il secondo elemento della struttura interna del
;;; polinomio.
;;; Simplify � semplicemente una composizione delle due "vere"
;;; funzioni di semplificazione, simplify-ms (che a sua volta
;;; "scarica" il lavoro a funzioni ausiliare) semplifica il singolo
;;; monomio invece sum-similar somma i simili. L'ordine � rilevante
;;; come la presenza di due simplify-ms poich� innanzitutto bisogna
;;; "pulire" i monomi da possibili variabili elevate a zero, in
;;; seguito si sommano i monomi simili. Infine si rivalutano i
;;; coefficienti se per caso nelle somme qualche coefficiente �
;;; diventato zero con la funzione simplify-c.
;;; Anche nel caso della funzione sum-power, come per la sum-similar,
;;; lasciano fare il "vero" lavoro ad una funzione che somma la lista
;;; ordinata in qesto modo continueranno a sommare finch� le
;;; "condizioni della somma" vengono rispettate. Appena non vengono
;;; pi� rispettate chiaramente avr� finito di sommare quell'elemento e
;;; quindi lo "metteranno nel risultato" e inizieranno a sommare il
;;; successivo. Invece quando due elementi vengono sommati la loro
;;; somma diventer� il nuovo elemnto della lista che verr� concatenato
;;; al resto della lista partendo dal terzo elemento.

(defun check-coeff (m)
  (if (zerop (second m)) (list 'm 0 0 ())
    m))

(defun simplify (ms)
  (simplify-c (sum-similar (simplify-ms ms))))

(defun simplify-ms (ms)
  (if (listp ms)
      (simplify-p (simplify-c ms))))

(defun simplify-c (ms)
  (cond ((null ms) nil)
	((zerop (second (first ms)))
	 (simplify-c (rest ms)))
	(T (cons (first ms) (simplify-c (rest ms))))))

(defun simplify-p (ms)
  (if (null ms) nil
    (let ((c (second (first ms)))
	  (td (third (first ms)))
	  (svs (sum-power (simplify-v
			   (fourth (first ms))))))
      (cons (list 'm c td svs)
	    (simplify-p (rest ms))))))
      
(defun simplify-v (vs)
  (cond ((null vs) nil)
	((= (second (first vs)) 0)
	 (simplify-v (rest vs)))
	(T (cons (first vs) (simplify-v (rest vs))))))

(defun sum-power (vs)
  (if (listp vs)
      (sum-sorted-p (sort vs #'string-lessp :key #'third))))

(defun sum-sorted-p (vs)
  (cond ((null vs) nil)
	((= (list-length vs) 1) (list (first vs)))
	((eq (third (first vs)) (third (second vs)))
	 (let ((p1 (second (first vs)))
	       (p2 (second (second vs)))
	       (sym (third (first vs))))
	   (sum-sorted-p (cons (list 'v (+ p1 p2) sym)
			       (rest (rest vs))))))
	(T (cons (first vs) (sum-sorted-p (rest vs))))))

(defun sum-similar (ms)
  (if (listp ms)
      (sum-sorted-m (sort ms #'monomial<))))

(defun sum-sorted-m (ms)
  (cond ((null ms) nil)
	((= (list-length ms) 1) (list (first ms)))
	((similar-m (first ms) (second ms))
	 (let ((c1 (second (first ms)))
	       (c2 (second (second ms)))
	       (td (third (first ms)))
	       (vps (fourth (first ms))))
	   (sum-sorted-m (cons (list 'm (+ c1 c2) td vps)
			       (rest (rest ms))))))
	(T (cons (first ms) (sum-sorted-m (rest ms))))))

;; Funzioni di di confronto, similar-m ritorna T quando due monomi
;; sono simili. check-v risulta vero quando la lista delle due
;; variabili ha gli stessi simboli, � effettivamente la funzione che
;; controlla che i monomi siano simili.

(defun similar-m (m1 m2)
  (let ((v1 (fourth m1))
	(v2 (fourth m2)))
    (if (= (list-length v1) (list-length v2))
	(check-v v1 v2))))

(defun check-v (v1 v2)
  (if (and (null v1) (null v2)) T
    (and (eq (third (first v1))
	     (third (first v2)))
         (= (second (first v1))
            (second (first v2)))
	 (check-v (rest v1) (rest v2)))))
   
;;; as-monomial Expression --> Monomial
;;; La funzione as-monomial ritorna la struttura dati (lista) che
;;; rappresenta il monomio risultante dal "parsing" dell'espressione
;;; Expression; il monomio risultante � ordinato secondo le
;;; specifiche.
;;; Tutti i primi controlli sono fatti per accettare espressioni del
;;; tipo: 42 o x o (expt x 3) e le loro controparti negative, escluso
;;; il caso -x poich� questa non � la semantica corretta del - in Lisp
;;; dato che di fatto � un unico simbolo. 

(defun as-monomial (exp)
  (let ((mon 
         (cond ((numberp exp) (list 'm exp 0 ()))
               ((symbolp exp) (list 'm 1 1 (list (list 'v 1 exp))))
               ((eq (first exp) 'expt) (list 'm 1 (third exp)
                                             (simplify-v
                                              (list (list 'v 
                                                          (third exp)
                                                          (second exp)
                                                          )))))
               ((and (eq (first exp) '-) 
                     (= (list-length (rest exp)) 1))
                (cond ((numberp (second exp))
                       (list 'm (-(second exp)) 0 ()))
                      ((symbolp (second exp))
                       (list 'm -1 1 (list 'v 1 (second exp))))
                      ((eq (first (first (rest  exp))) 'expt)
                       (let ((pow (third (first (rest exp))))
                             (sym (second (first (rest exp)))))
                         (list 'm -1 pow (simplify-v 
                                          (list (list 'v pow sym)
                                                )))))))
               ((eq (first exp) '*)
                (let ((c (extract-c exp))
                      (vps (sum-power (simplify-v
                                       (sort (extract-vps exp) 
                                             #'string-lessp
                                             :key #'third)))))
                  (check-coeff (list 'm c (calculate-td vps) vps))))
               ((compute-if-possible exp) (list 'm (eval exp) 0 ())))))
    (if (is-monomial mon) mon
      (error "monomio non valido"))))
    

;;; Funzioni di appoggio
;;
;; extract-c Expression --> Coefficient
;; La funzione ritorna il coefficiente dall'espressione, nel caso il
;; coefficiente pu� calcolarlo, lo verifica attraverso la funzione
;; compute-if-possible, ritorna il valore del coefficiente calcolato
;; altrimenti ritorna 1 perch� vuol dire che l'elemento nella posizione
;; in cui pu� essere il coefficiente in realt� � una variabile e quindi
;; il coefficiente � 1.

(defun extract-c (exp)
  (if (compute-if-possible (second exp))
      (eval (second exp))
    1))

;; compute-if-possible
;; Risulta vero quando pu� calcolare l'elemento passato.

(defun compute-if-possible (e)
  (let ((answ (first ((lambda (x) (multiple-value-bind (v err)
                                      (ignore-errors (numberp (eval x)))
                                    (list v err)))
                      e))))
    (if (null answ) nil
      T)))

;; extract-vps Expression --> VP-list
;; La funzione ritorna la lista di Variable come definite
;; precedentemente. Effettua di nuovo un controllo per sapere se il
;; secondo elemento dell'espressione � un coefficiente o una
;; variabile. 

(defun extract-vps (exp)
  (if (compute-if-possible (second exp))
      (parse-v (rest (rest exp)))
    (parse-v (rest exp))))

;; parse-v Expression --> VP-list
;; Funzione che di fatto fa il lavoro di costruire la lista di
;; VarPowers.

(defun parse-v (exp)
  (if (null exp) nil
    (cond ((symbolp (first exp))
	   (append (list (append '(v 1) (list (first exp))))
		   (parse-v (rest exp))))
	  ((and (= (list-length (first exp)) 3)
		(eq (first (first exp)) 'expt))
	   (append (list (append '(v) 
				 (list (third (first exp))) 
				 (list (second (first exp)))))
                   (parse-v (rest exp))))
          (T (error "espressione non valida")))))

;; calculate-td VP-list --> td
;; Funzione che ritorna il total-degree del monomio cio� somma gli
;; esponenti delle varie variaibli.

(defun calculate-td (vps)
  (if (null vps) 0
    (+ (second (first vps)) (calculate-td (rest vps)))))

;;; as-polynomial Expression --> Polynomial
;;; La funzione as-polynomial ritorna la struttura dati (lista) che
;;; rappresenta il monomio risultante dal "parsing" dell'espressione
;;; Expression; il polinomio risultante � ordinato secondo le
;;; specifiche.
;;; L'else dell'if � effettuato per accettare espressioni che
;;; identificano un solo monomio, che comunque � un polinomio, e nel
;;; caso venga passato un'espresisone non valida fallir� l'as-monomial
;;; e quindi anche l'as-polynomial.  
;;; E' necessario mettere (list (as-monomial exp)) perch� altrimenti
;;; non si otterrebbe la lista di monomi(che � uno) ma solo un monomio
;;; e questo darebbe problemi nell'applicazione di tutte le altre
;;; funzioni. 

(defun as-polynomial (exp)
  (let ((polynomial 
         (if (and (listp exp) (eq (first exp) '+))
             (let ((sorted-monomials
                    (simplify (sort (extract-m (rest exp)) 
                                    #'monomial<))))
               (list 'poly sorted-monomials))
           (list 'poly (simplify 
                        (list (as-monomial exp)))))))
    (if (is-polynomial polynomial) polynomial
      (error "polinomio non valido"))))

;; extract-m, funzione di appoggio che per ogni elemento della lista
;; lo parsa generando il monomio in struttura interna associato.

(defun extract-m (ms)
  (if (null ms) nil
    (cons (as-monomial (first ms))
	    (extract-m (rest ms)))))

;;; Funzioni per l'ordinamento dei monomi nella struttura del
;;; polinomio, monomial< viene utilizzato per il sort e variable<
;;; viene utilizzato da monomial< per valutare quale monomio � primo
;;; di un altro in base all'ordine lessicografico delle variabili che
;;; li compongono.

(defun monomial< (m1 m2)
  (cond ((< (third m1) (third m2)) T)
        ((> (third m1) (third m2)) nil)
        ((= (third m1) (third m2)) 
         (variable< (fourth m1) (fourth m2)))
        ))

(defun variable< (v1 v2)
  (cond ((null v1) T)
        ((null v2) nil)
        ((string-lessp (third (first v1)) 
                       (third (first v2))) T)
        ((eq (third (first v1))
             (third (first v2))) 
         (cond ((= (second (first v1)) 
		   (second (first v2)))
                (variable< (rest v1) (rest v2)))
               ((< (second (first v1))
                   (second (first v2))) T)
               (T nil)))
        (T nil)))

;;; transform
;;; Funzione utilizzata per rendere flessibili gli input di tutte le
;;; funzioni che seguono , cio� che possano accettare sia delle
;;; espressioni, secondo la sintassi Common Lisp, sia dei monomi
;;; rappresentati nella struttura interna e sia i polinomi in
;;; struttura interna. Si suppone, come detto pi� volte in classe e
;;; sul forum, che se vengano passato monomi o polinomi in struttura
;;; interna questi sono stati costruiti attraverso gli appositi
;;; predicati e quindi non hanno bisogno di controllo eccessivi(e di
;;; operzioni di semplficazione e ordinamento) a parte la valutazione
;;; del primo elemento della lista per stabilire se si sta parlando di
;;; un monomio o di un polinomio.

(defun transform (exp)
  (cond ((and (listp exp) (eq (first exp) 'poly))
	 exp)
	((and (listp exp) (eq (first exp) 'm))
	 (list 'poly (list exp)))
	(T (as-polynomial exp))))

;;; pprint-polynomial
;;; simplify non � necessario poich� se il polinomio o il monomio
;;; arriva gi� parsato significa che ha gi� "subito" i predicati di
;;; semplificazione. Invece se viene passata un'espressione questa
;;; passer� in as-polynomial e quindi verr� semplificato

(defun pprint-polynomial (epoly)
  (let ((poly (transform epoly)))
    (pprint-mons (second poly))))

;; Funzioni di appoggio, effettuano stampe formattate.

(defun pprint-mons(mons)
  (cond ((null mons) (print "null"))
        ((= (list-length mons) 1)
	 (pprint-mon (first mons)))
        ((> (list-length mons) 1)
	 (pprint-mon (first mons))
	 (pprint-mons (rest mons)))))
        
(defun pprint-mon(m)
  (if (< (second m) 0)
      (format t "~S " (second m)) 
    (format t "+ ~S " (second m))) 
  (if (eql (first (fourth  m)) NIL)
      (format t "") 
    (pprint-vars (fourth m))))

(defun pprint-vars(v)
  (cond ((null v) (format t ""))
        (T (format t "* ~S^~S "
		   (third (first v))
		   (second (first v)))
	   (pprint-vars (rest v)))))

;;; coefficients Poly --> Coefficients
;;; La funzione coefficients ritorna una lista Coefficients dei
;;; coefficienti di Poly. Lascia fare il "vero" lavoro a get-coeff che
;;; � la funzione di appoggio che estrae i coefficienti e ne
;;; costruisce la lista.

(defun coefficients (epoly)
  (let ((poly (transform epoly)))
    (if (null (second poly)) '(0)
      (get-coeff(second poly)))))

;; Funzione di appoggio

(defun get-coeff (m)
  (if (null m) nil
    (cons (second (first m))
	  (get-coeff (rest m)))))

;;; variables Poly --> Variables
;;; La funzione variables ritorna una lista Variables dei simboli di
;;; variabile che apaiono in Poly.
;;; E' necessario un sort dopo la funzione remove-duplicates poich�
;;; questa funzione non sempre elimina le occorrenze di un elemento
;;; dopo la prima volta che compare ergo c'� il rischio che delle
;;; liste del tipo (a d f a) diventino (d f a), � quindi chiaro che
;;; poi � necessario riordinare la lista.

(defun variables (epoly)
  (let ((poly (transform epoly)))
    (let ((vs (vars-to-symbol
	       (list-vars (second poly)))))
      (sort (remove-duplicates vs) #'string-lessp))))

;; Funzioni di appoggio
;; list-vars crea una lista di tutte le variaibli rappresentate nella
;; rappresentazione interna. Invece vars-to-symbol estrare da ogni
;; struttura var-power il simbolo. La rimozione di duplicati �
;; lasciata a variables.

(defun list-vars (m)
  (if (null m) nil
    (append (fourth (first m))
	    (list-vars (rest m)))))

(defun vars-to-symbol(m)
  (if(null m) nil 
    (cons (third (first m))
	  (vars-to-symbol (rest m)))))

;;; monomials Poly --> Monomials
;;; La funzione monomials ritorna la lista ordinata dei monomi che
;;; appaiono in Poly. La lista dei monomi ordinati � semplicemente il
;;; secondo elemento della struttura poly. Non sono necessari
;;; ulteriori controlli perch� la funzione transform gi� effettua quel
;;; lavoro.

(defun monomials (epoly)
  (let ((poly (transform epoly)))
    (second poly)))
  
;;; maxdegree Poly --> Degree
;; La funzione maxdegree ritorna il massimo grado dei monomi che
;; appaiono in Poly.
;; Date le specifiche dell'ordinamento il massimo grado sar� il td
;; dell'ultimo monomio della lista che compone il polinomio.
;; In maxdegree c'� un first in pi� perch� la funzione last
;; restituisce la lista dell'ultimo elemtno ((M C TD Vars)) 
;; quindi � necessario dirgli di guardare il primo ovvero
;; (M C TD Vars) dal quale prendere il terzo elemtno della lista. 

(defun maxdegree (epoly)
  (let ((poly (transform epoly)))
    (third (first (last (second poly))))))

;;; mindegree Poly --> Degree
;; La funzione mindegree ritorna il minimo grado dei monomi che
;; appaiono in Poly.
;; Il discorso � analogo a maxdegree in questo caso per� sara il td
;; del primo monomio.

(defun mindegree (epoly)
  (let ((poly (transform epoly)))
    (third (first (second poly)))))

;;; polyplus Poly1 Poly2 --> Result
;;; La funzione polyplus produce il polinomio somma di Poly1 e Poly 2
;;; L'algoritmo di somma � ampiamente spiegato nei commenti del
;;; predicato omonimo in Prolog.

(defun polyplus (epoly1 epoly2) 
  (let ((ms (sort (simplify (append (second (transform epoly1))
				    (second (transform epoly2))))
		  #'monomial<)))
    (list 'poly ms)))

;;; polyminus Poly1 Poly2 --> Result
;;; La funzione polyminus produce il polinomio differenza di Poly1 e
;;; Poly2.
;;; Il ragionamento � stato descritto nei commenti di Prolog.

(defun polyminus (epoly1 epoly2)
  (polyplus (transform epoly1)
	    (list 'poly (negative-cmons
			 (second (transform epoly2))))))
	    
;;; Funzioni di appoggio, servono per negare i coefficienti del
;;; secondo monomio.
;;; Negative-cmons chiama ricorsivamente negative-cmon che �
;;; effettivamente la funzione che calcola l'opposto di quel
;;; coefficiente. 

(defun negative-cmons (ms)
  (if (null ms) nil
    (cons (negative-cmon (first ms)) (negative-cmons (rest ms)))))

(defun negative-cmon (m)
  (if (null m) nil
    (let ((m (first m))
          (c (- 0 (second m)))
          (td (third m))
          (vars (fourth m)))
      (list m c td vars))))

;;; polytimes Poly1 Poly2 --> Result
;;; La funzione polytimes ritorna il polinomio risultante dalla
;;; moltiplicazione di Poly1 e Poly2.
;;; L'idea � analoga a quella del Prolog ovvero per prodotto si
;;; intende appendere la lista di variabili di ogni monomio del primo
;;; polinomio con ogni monomio del secondo polinomio e poi lasciar fare
;;; tutto il resto del lavoro al predicato di semplificazione simplify.

(defun polytimes (epoly1 epoly2)
  (let ((mss (extract (second (transform epoly1))
		      (second (transform epoly2)))))
    (list 'poly (simplify mss))))

;; Funzioni di appoggio per appendere le liste di varaibili come
;; descritto precedentemente.

(defun extract (ms1 ms2)
  (if (null ms1) nil 
    (append (product (first ms1) ms2)
	    (extract (rest ms1) ms2))))

(defun product (m1 ms)
  (if (null ms) nil         
    (let ((m (first m1))
	  (c (* (second m1)(second (first ms))))
	  (td (+ (third m1) (third (first ms))))
	  (vs (append (fourth m1) (fourth (first ms)))))
      (cons (list m c td vs) (product m1 (rest ms))))))

;;; polyval Polynomial VariableValues --> Value
;;; La funzione polyval restitutisce il valore Value del polinomio
;;; Polynomial (che pu� anche essere un monomio), nel punto
;;; n-dimensionale rappresentato dalla lista Variable-Values, che
;;; contiene un valore per ogni variabile ottenuta con la funzione
;;; variables. 

;;; sub-ms     sum-values
(defun polyval (epoly varvalue)
  (let ((poly (transform epoly)))
    (let ((vs (variables poly)))
      (if (= (list-length vs) (list-length varvalue))
          (let ((ms (sub-ms (second poly) vs varvalue)))
            (sum-values ms))
        (error "Input non valido"))))) 
          
;; Funzioni di appoggio
;; sub-ms richiama ricorsivamente sub-m per ogni monomio passandogli
;; lista di variabili(ottenuta dalla chiamata di varaibles in polyval)
;; e valori varvalue.

(defun sub-ms (ms vs vv)
  (if (null ms) nil 
    (cons (multiplay (sub-m (first ms) vs vv))
            (sub-ms (rest ms) vs vv)))) 

;; sub-m sostituisce i simboli di variabili ai valori associati, per
;; farlo chiama ricorsivamente sub-v che � la funzione che
;; effettivamente effettua le sostituzioni.
;; Il caso base (null vs) ritorna (list (second m)) questo perch� si
;; necessita della lista di un elemento per ottenere il risultato
;; dell'append corretto. Poich� (append '(1 2) 3) -> (1 2 . 3), invece
;; (append '(1 2) '(3)) -> (1 2 3) che � il risultato che si vuole.

(defun sub-m (m vs vv)
  (if (null vs) (list (second m))
    (append (sub-v (fourth m) (first vs) (first vv)) 
            (sub-m  m  (rest vs) (rest vv)))))

(defun sub-v (mv s v)
  (cond ((null mv) nil)
        ((eql (third (first mv)) s) 
         (cons (expt v (second (first mv)))
	       (sub-v (rest mv) s v)))
        (T (sub-v (rest mv) s v))))

;; multiplay, moltiplica ricorsivamente tutti gli elementi della
;; lista, si noti che nella lista � presente anche il coefficiente che
;; � stato aggiunto con il caso base di sub-m.

(defun multiplay (values)
  (if (null values) 1
    (* (first values) (multiplay (rest values)))))

;; sum-values, somma i valori ottenuti dal calcolo dei vari monomi che
;; compongono il polinomio.

(defun sum-values (values)
  (if (null values) 0
    (+ (first values) (sum-values (rest values)))))

;;;; end of file -- mvpoli.lisp
