mvpoli.lisp

Introduzione:
Questa libreria permette di manipolare polinomi multivariati attraverso una serie di funzioni successivamente descritte.
Genericamente queste operazioni permettono di ottenere informazioni riguardo il polinomio di interesse, effettuare calcoli di vario genere tra polinomi oppure convertire
delle espressioni che denotano dei polinomi nella rappresentazione interna del sistema.

Come caricare la libreria:
LispWorks 6.1.1
Esistono due modi per caricare la libreria in LispWorks:
1. Utilizzare la funzione load dal listener. Questa funzione si aspetta un argomento che � la stringa del percorso tra le directories per accedere al file che si vuole caricare,
   in questo caso mvpoli.lisp. Il percorso, anche sotto Windows, segue le convenzioni unix per la specifica di percorsi.
   Una volta che � stata utilizzata questa funzione si potr� utilizzare liberamente la libreria.
   Esempio: (load "d:/mvpoli.lisp")
2. Utilizzare l'interfaccia grafica. Per farlo bisogner� aprire il file della libreria attraverso il percorso: File->Open... quindi si apre l'editor contenente il codice della
   libreria(si prega di non modificarlo) allora bisogner� utilizzare il comando compile buffer(� un'icona) presente nell'editor per compilare la libreria. Allora si potr�
   utilizzare la libreria attraverso il listener.


Specifiche della libreria

Rappresentazione interna:
Variabili: sono liste del tipo (v power var-symbol) dove v indica che � un lista che denota una variabile, power � l'esponenete (>= 0) a cui � elevato il simbolo della variaible
	   e var-symbol � il simbolo della variabile. Questo simbolo pu� essere un sola lettera oppure, pi� in generale, una qualsiasi combinazione di lettere, numeri e simboli
	   che se testati con la funzione di sistema symbolp si ottiene il valore T.
	   Esempi: CL-USER 3 > (is-varpower '(v 3 t3t))
	       	  	       T
	           CL-USER 4 > (is-varpower '(v 3 t-+-t))
			       T
	           CL-USER 5 > (is-varpower '(v 3 a))
		   	       T
			La funzione is-varpower � descritta in seguito ma qua utilizzata per dare degli esempi di simboli di variabili accettati.
Monomi: sono liste del tipo (m coefficient total-degree vars-n-powers) dove m indica che � una lista che denota un monomio, coefficient indica il coefficiente del monomio,
	total-degree indica il grado totale del monomio,dovr� essere maggiore o uguale a zero, e vars-n-powers � la lista di variaibli che compongono il monomio
	e sono del tipo sopra descritto.
Polinomi: sono liste del tipo (poly monomials) dove poly indica che � una lista che denota un polinomio e monomials � la lista di monomi che compongono il polinomio i quali
	  sono tutti del tipo precedentemente descritto.

Funzioni:
Si noti che per tutte le scelte implementative e algoritmiche bisogna fare riferimento ai commenti scritti nel codice della libreria.

is-varpower Variable -> Boolean
  risulta T quando Variable � una variabile � una variabile scritta nella rappresentazione interna.
  	   Esempi: CL-USER 3 > (is-varpower '(v 3 t3t))
	       	  	       T
	           CL-USER 4 > (is-varpower '(v 3 t-+-t))
			       T
	           CL-USER 5 > (is-varpower '(v 3 a))
		   	       T
		   CL-USER 6 > (is-varpower '(v t a))
		   	       NIL
		   CL-USER 7 > (is-varpower '(v t 5))
		   	       NIL
is-monomial Monomial -> Boolean
  risulta T quando Monomial � un monomio scritto nella rappresentazione interna. Non effettua controlli numerici sul total-degree e l'effettiva somma degli esponenti
  delle variabili. Questa � una funzione che semplicemente controlla se la lista rispetta la rappresentazione interna da un punto di vista simbolico e non matematico.
  	Esempi: CL-USER 8 > (is-monomial '(m 3 84 ((v 42 a) (v 42 pippo)))) 
		   	    T
		CL-USER 9 > (is-monomial '(m 3 42 ((v 42 a) (v 42 pippo)))) 
			    T
	        CL-USER 10 > (is-monomial '(m 3 -42 ((v 42 a) (v 42 pippo)))) 
			     NIL
is-polynomial Polynomial -> Boolean
  risulta T quando Polynomial � un polinomio scritto nella rappresentazione interna, e ogni monomio che lo compone � un monomio scritto secondo la rappresentazione interna.
  	  Esempi: CL-USER 12 > (is-polynomial '(poly ((m 3 3 ((v 3 r))) (m 42 0 ()))))
	  	  	       T
		  CL-USER 13 > (is-polynomial '(ply ((m 3 3 ((v 3 r))) (m 42 0 ()))))
		  	       NIL
as-monomial Expression -> Monomial
  la funzione as-monomial ritorna la struttra dati (lista) che rappresenta il monomio risultante dal "parsing" dell'espressione Expression.
  Expression deve essere un'espressione secondo l'utilizzo, e la scrittura, delle funzioni di CL. Il monomio ottenuto in rappresentazione interna sar� ordinato in ordine
  lessicografico crescente delle variaibli. Si noti che questa funzione effettua tutte le semplificazioni del caso riguardo il coefficiente(valuta se � uguale a zero)
  o variabili con lo stesso simbolo. Il coefficiente, se presente altrimenti si sottointende che sia 1, pu� essere dato da una qualsiasi espressione la cui computazione
  restituisce un numero e deve essere inserito come primo elemento dell'espressione dopo il simbolo di moltiplicazione.
    	      Esempi: CL-USER 14 > (as-monomial '(* 42 pippo pluto (expt x 42)))
		 	 	   (M 42 44 ((V 1 PIPPO) (V 1 PLUTO) (V 42 X)))
		      CL-USER 15 > (as-monomial '(* pippo pluto (expt x 42)))
			 	   (M 1 44 ((V 1 PIPPO) (V 1 PLUTO) (V 42 X)))
		      CL-USER 16 > (as-monomial 42)
			 	   (M 42 0 NIL)
		      CL-USER 17 > (as-monomial '(* pippo pippo (expt x 42)))
		      	      	   (M 1 44 ((V 2 PIPPO) (V 42 X)))
 		      CL-USER 20 > (as-monomial 'x)
		      	      	   (M 1 1 ((V 1 X)))
as-polynomial Expression -> Polynomial
  la funzione as-polynomial ritorna la struttura dati (lista) che rappresenta il polinomio risultante dal "parsing" dell'espressione Expression. Anche in questo caso Expression
  deve essere un'espressione scritta secondo le "regole di scrittura" delle funzioni di CL. I monomi che compongono il polinomio saranno ordinati in ordine crescente in base ai
  loro rispettivi total-degree con eventuali spareggi determinati dalle variaibli che li compongono. Inoltre se due monomi sono composti dalle stesse variaibli l'ordinamento
  � in ordine crescente rispetto alle combinazioni variabile/esponente. Anche in questo caso il polinomio risultante avr� subito tutte le semplificazioni del caso.
       	      Esempi: CL-USER 24 > (as-polynomial '(+ 42 (* x (expt y 2)) (* (cos 42) pluto) (* 41 x (expt y 2)) (* 0 a s d))) 
	      	      	      	   (POLY ((M 42 0 NIL) (M -0.3999853 1 ((V 1 PLUTO))) (M 42 3 ((V 1 X) (V 2 Y)))))
		      CL-USER 25 > (as-polynomial 42)
		      	      	   (POLY ((M 42 0 NIL)))
varpowers Monomial -> VP-list
  data una struttura Monomial, ritorna la lista di varpowers VP-list.
       	   Esempio: CL-USER 28 > (varpowers '(m 3 4 ((v 1 a) (v 2 g) (v 1 x))))
	   	    	       	 ((V 1 A) (V 2 G) (V 1 X))
vars-of Monomial -> Variables
  data una struttura Monomial, ritorna la lista di variabili Variables.
       	   Esempio: CL-USER 29 > (vars-of '(m 3 4 ((v 1 a) (v 2 g) (v 1 x))))
	   	    	       	 (A G X)
monomial-degree Monomial -> TotalDegree
  data una struttura Monomial, ritorna il suo grado totale TotalDegree
       	   Esempio: CL-USER 30 > (monomial-degree '(m 3 4 ((v 1 a) (v 2 g) (v 1 x))))
	   	    	       	 4
monomial-coefficient Monomial -> Coefficient
  data una struttura Monomial, ritorna il suo coefficiente Coefficient.
       	   Esempio: CL-USER 31 > (monomial-coefficient '(m 3 4 ((v 1 a) (v 2 g) (v 1 x))))
	   	    	       	 3

Tutte le funzioni elencate di seguito accettano come argomenti sia polinomi e monomi in rappresentazione interna ma anche espressioni di CL che denotano polinomi o monomi.

pprint-polynomial Polynomial -> NIL
  la funzione pprint-polynomial ritorna NIL dopo aver stampato (sullo "standard output") una rappresentazione tradizionale del termine polinomio associato a Polynomial.
     	      Esempi: CL-USER 26 > (pprint-polynomial '(POLY ((M 42 0 NIL) (M -0.3999853 1 ((V 1 PLUTO))) (M 42 3 ((V 1 X) (V 2 Y))))))
	      	      	      	   + 42 -0.3999853 * PLUTO^1 + 42 * X^1 * Y^2 
				   NIL
		      CL-USER 27 > (pprint-polynomial '(+ (expt f 3) (* (+ 3 5 -9) a r (expt g 4))))
		      	      	   + 1 * F^3 -1 * A^1 * G^4 * R^1 
				   NIL
coefficients Poly -> Coefficients
  la funzione coefficients ritorna una lista Coefficients dei coefficienti di Poly. I coefficienti sono nell'ordine in cui essi appaiono nella rappresentazione interna del
  polinomio.
     	      Esempio: CL-USER 34 > (coefficients '(+ 42 (* -1 a (expt t 5)) (* 4 y) (* 3/4 z)))
	      	       	       	    (42 4 3/4 -1)
variables Poly -> Variables
  la funzione variables ritorna una lista Variables dei simboli di variabile che appaiono in Poly. Le variabili sono ordinate in ordine lessicografico crescente e le
  ripetizioni di simboli di variabile sono omessi.
     	      Esempio: CL-USER 35 > (variables '(+ 42 (* -1 a z x pluto qd (expt t 5)) (* 4 y) (* 3/4 z)))
	      	       	       	    (A PLUTO QD T X Y Z)
monomials Poly -> Monomials
  la funzione monomial ritorna la lista dei monomi che appaiono in Poly. I monomi appariranno nell'ordine in cui essi sono presenti in poly, quindi sono ordinati secondo
  le specifiche dell'ordinamento di as-polynomial.
     	      Esempio: CL-USER 37 > (monomials '(+ 42 (* -1 a (expt t 5)) (* 4 y) (* 3/4 z)))
	      	       	       	    ((M 42 0 NIL) (M 4 1 ((V 1 Y))) (M 3/4 1 ((V 1 Z))) (M -1 6 ((V 1 A) (V 5 T))))
maxdegree Poly -> Degree
  la funzione maxdegree ritorna il massimo grado dei monomi che appaiono in Poly.
     	      Esempio: CL-USER 38 > (maxdegree '(+ 42 (* -1 a z x pluto qd (expt t 5)) (* 4 y) (* 3/4 z)))
	      	       	       	    10
mindegree Poly -> Degree
  la funzione mindegree ritorna il minimo grado dei monomi che appaiono in Poly.
     	      Esempio: CL-USER 39 > (mindegree '(+ 42 (* -1 a z x pluto qd (expt t 5)) (* 4 y) (* 3/4 z)))
	      	       	       	    0
polyplus Poly1 Poly2 -> Result
  la funzione polyplus produce il polinomio somma di Poly1 e Poly2. Il polinomio somma � ancora rappresentato in rappresentazione interna, se lo si vuole in forma tradizionale
  bisogner� stampare il risultato di polyplus attraverso la funzione pprint-polynomial. Il polinomio ottenuto rispetta ancora le specifiche di ordinamento.
  	    Esempio: CL-USER 3 > (polyplus '(+ (* 3 x y v) (* 42 (expt a 0) f)) '(+ 42 (* 3 f) (* 4 x v y)))
	    	     	       	 (POLY ((M 42 0 NIL) (M 45 1 ((V 1 F))) (M 7 3 ((V 1 V) (V 1 X) (V 1 Y)))))
		     CL-USER 4 > (pprint-polynomial *)
		     	       	 + 42 + 45 * F^1 + 7 * V^1 * X^1 * Y^1 
				 NIL
polyminus Poly1 Poly2 -> Result
  la funzione polyminus produce il polinomio differenza di Poly1 e Poly2. Il polinomio differenza � ancora rappresentato in rappresentazione interna, se lo si vuole in forma
  tradizionale bisogner� stampare il risultato di polyminus attraverso la funzione pprint-polynomial. Il polinomio ottenuto rispetta ancora le specifiche di ordinamento.
  	       Esempio: CL-USER 8 > (polyminus '(+ (* 3 x y v) (* 42 (expt a 0) f)) '(+ 42 (* 3 f) (* 4 x v y)))
	       			    (POLY ((M -42 0 NIL) (M 39 1 ((V 1 F))) (M -1 3 ((V 1 V) (V 1 X) (V 1 Y)))))	
			CL-USER 9 > (pprint-polynomial *)
				    -42 + 39 * F^1 -1 * V^1 * X^1 * Y^1 
				    NIL
polytimes Poly1 Poly2 -> Result
  la funzione polytimes ritorna il polinomio risultante dalla moltiplicazione di Poly1 e Poly2. Il polinomio prodotto � ancora rappresentato in rappresentazione interna,
  se lo si vuole in forma tradizionale bisogner� stampare il risultato di polytimes attraverso la funzione pprint-polynomial. Il polinomio ottenuto rispetta ancora
  le specifiche di ordinamento.
     		Esempi: CL-USER 16 > (polytimes '(+ (* 3 f) (* 2 (expt a 3))) '(+ (* a b) (* 3 f) (* 4 (expt a 3))))
				     (POLY ((M 9 2 ((V 2 F))) (M 3 3 ((V 1 A) (V 1 B) (V 1 F))) (M 18 4 ((V 3 A) (V 1 F)))
				     (M 2 5 ((V 4 A) (V 1 B))) (M 8 6 ((V 6 A)))))
			CL-USER 17 > (pprint-polynomial *)
				     + 9 * F^2 + 3 * A^1 * B^1 * F^1 + 18 * A^3 * F^1 + 2 * A^4 * B^1 + 8 * A^6 
				     NIL

		        CL-USER 18 > (polytimes '(+ (* 3 a) (* 2 (expt a 3))) '(+ (* a b) (* 3 f) (* 4 (expt a 3))))
				     (POLY ((M 9 2 ((V 1 A) (V 1 F))) (M 3 3 ((V 2 A) (V 1 B))) (M 6 4 ((V 3 A) (V 1 F)))
				     (M 12 4 ((V 4 A))) (M 2 5 ((V 4 A) (V 1 B))) (M 8 6 ((V 6 A)))))
		        CL-USER 19 > (pprint-polynomial *)
				     + 9 * A^1 * F^1 + 3 * A^2 * B^1 + 6 * A^3 * F^1 + 12 * A^4 + 2 * A^4 * B^1 + 8 * A^6 
				     NIL
				Nell'esempio l'output � stato mandato a capo per leggibilit�.
polyval Polynomial VariableValues -> Value
  la funzione polyval restituisce il valore Value del polinomio Polynomial nel punto n-dimensionale rappresentato dalla lista VariableValues, che contiene un valore
  per ogni variabile ottenuta con la funzione variables. La lista deve essere lunga come la lista ottenuta dalla funzione variables e sostituir� il primo elemento di
  VariableValues alla prima variaible, della lista ottenuta con variables, all'interno di Polynomial e cos� via.
  		 Esempio: CL-USER 20 > (polyval '(+ (* 3 a x) (* (+ 42 -40) (expt y 3) x)) '(2 3 1))
		 	  	       24
